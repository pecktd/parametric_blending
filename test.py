# System Modules
from audioop import mul
from imp import reload

# Maya Modules
import maya.cmds as mc

# pkrig Modules
from pkrig2 import core as prc
from pkrig2 import utils as pru
from pkrig2 import naming_tools as prnt


def first_coeff(param):

    pow3 = prc.create('multiplyDivide')
    pow3.attr('op').v = prc.Operator.power
    param >> pow3.attr('i1x')
    pow3.attr('i2x').v = 3

    mult = prc.create('multiplyDivide')
    pow3.attr('ox') >> mult.attr('i2x')
    pow3.attr('ox') >> mult.attr('i2y')
    pow3.attr('ox') >> mult.attr('i2z')

    return mult


def second_coeff(param):

    sub = prc.create('plusMinusAverage')
    sub.attr('op').v = prc.Operator.subtract
    sub.add(ln='default', dv=1, k=True)
    sub.attr('default') >> sub.attr('i1').last()
    param >> sub.attr('i1').last()

    pow2 = prc.create('multiplyDivide')
    pow2.attr('op').v = prc.Operator.power
    param >> pow2.attr('i1x')
    pow2.attr('i2x').v = 2

    mult3 = prc.create('multDoubleLinear')
    pow2.attr('ox') >> mult3.attr('i1')
    mult3.attr('i2').v = 3

    multall = prc.create('multDoubleLinear')
    mult3.attr('o') >> multall.attr('i1')
    sub.attr('o1') >> multall.attr('i2')

    mult = prc.create('multiplyDivide')
    multall.attr('o') >> mult.attr('i2x')
    multall.attr('o') >> mult.attr('i2y')
    multall.attr('o') >> mult.attr('i2z')

    return mult


def third_coeff(param):

    sub = prc.create('plusMinusAverage')
    sub.attr('op').v = prc.Operator.subtract
    sub.add(ln='default', dv=1, k=True)
    sub.attr('default') >> sub.attr('i1').last()
    param >> sub.attr('i1').last()

    pow2 = prc.create('multiplyDivide')
    pow2.attr('op').v = prc.Operator.power
    sub.attr('o1') >> pow2.attr('i1x')
    pow2.attr('i2x').v = 2

    mult3 = prc.create('multDoubleLinear')
    param >> mult3.attr('i1')
    mult3.attr('i2').v = 3

    multall = prc.create('multDoubleLinear')
    mult3.attr('o') >> multall.attr('i1')
    pow2.attr('ox') >> multall.attr('i2')

    mult = prc.create('multiplyDivide')
    multall.attr('o') >> mult.attr('i2x')
    multall.attr('o') >> mult.attr('i2y')
    multall.attr('o') >> mult.attr('i2z')

    return mult


def fourth_coeff(param):

    sub = prc.create('plusMinusAverage')
    sub.attr('op').v = prc.Operator.subtract
    sub.add(ln='default', dv=1, k=True)
    sub.attr('default') >> sub.attr('i1').last()
    param >> sub.attr('i1').last()

    pow3 = prc.create('multiplyDivide')
    pow3.attr('op').v = prc.Operator.power
    sub.attr('o1') >> pow3.attr('i1x')
    pow3.attr('i2x').v = 3

    mult = prc.create('multiplyDivide')
    pow3.attr('ox') >> mult.attr('i2x')
    pow3.attr('ox') >> mult.attr('i2y')
    pow3.attr('ox') >> mult.attr('i2z')

    return mult


def main():
    
    poses = [(-7.230299019694844, 0.0, 8.741987123540689),
        (-10.036349920815782, 2.7143136075379726, 0.7906745294635336),
        (-2.7931162138716736, -1.7489454154962116, -5.0069338565260075),
        (3.2244178711354756, 7.942308735861349, 3.5390415094101257)]
    dotno = 200

    cvs = []
    for pos in poses:
        cv = prc.Locator()
        cv.attr('t').v = pos
        cvs.append(cv)

    for ix in range(dotno):
        dot = prc.Dag(mc.sphere(ch=False)[0])
        dot.attr('s').v = (0.5, 0.5, 0.5)

        if ix == 0:
            cvs[0].attr('t') >> dot.attr('t')
            continue
        elif ix == (dotno - 1):
            cvs[-1].attr('t') >> dot.attr('t')
            continue
        
        currparam = float(ix + 1)/float(dotno)
        dot.add(ln='id', dv=ix, k=True)
        dot.add(ln='param', dv=currparam, k=True)

        coeff1 = first_coeff(dot.attr('param'))
        coeff2 = second_coeff(dot.attr('param'))
        coeff3 = third_coeff(dot.attr('param'))
        coeff4 = fourth_coeff(dot.attr('param'))
        
        cvs[3].attr('t') >> coeff1.attr('i1')
        cvs[2].attr('t') >> coeff2.attr('i1')
        cvs[1].attr('t') >> coeff3.attr('i1')
        cvs[0].attr('t') >> coeff4.attr('i1')

        sum_ = prc.create('plusMinusAverage')
        coeff1.attr('o') >> sum_.attr('i3').last()
        coeff2.attr('o') >> sum_.attr('i3').last()
        coeff3.attr('o') >> sum_.attr('i3').last()
        coeff4.attr('o') >> sum_.attr('i3').last()

        sum_.attr('o3') >> dot.attr('t')
